package logx

import (
	"os"
	"time"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"gopkg.in/natefinch/lumberjack.v2"
)

/*
生成log
*/
type Options struct {
	//分割配置
	Filename   string // 日志文件路径，默认 os.TempDir()
	MaxSize    int    // 每个日志文件保存10M，默认 100M
	MaxBackups int    // 保留30个备份，默认不限
	MaxAge     int    // 保留7天，默认不限
	Compress   bool   // 是否压缩，默认不压缩

	Level        string
	Formatter    string //json or txt
	OutType      string //输出到哪 console all file
	HasTimestamp bool

	Caller      bool //启用堆栈
	Development bool // 记录行号

}

func New(opt Options) *MyLog {
	var (
		encoder zapcore.Encoder     = nil
		write   zapcore.WriteSyncer = nil
		level   zapcore.Level       = zapcore.InfoLevel
	)
	encoderConfig := zapcore.EncoderConfig{
		TimeKey:       "time",
		LevelKey:      "level",
		NameKey:       "logger",
		CallerKey:     "linenum",
		MessageKey:    "msg",
		StacktraceKey: "stacktrace",
		LineEnding:    zapcore.DefaultLineEnding,
		EncodeLevel:   zapcore.LowercaseLevelEncoder, // 小写编码器
		// EncodeLevel: zapcore.LowercaseColorLevelEncoder, // 小写编码器

		EncodeTime:     zapcore.ISO8601TimeEncoder,     // ISO8601 UTC 时间格式
		EncodeDuration: zapcore.SecondsDurationEncoder, //
		EncodeCaller:   zapcore.FullCallerEncoder,      // 全路径编码器
		EncodeName:     zapcore.FullNameEncoder,
	}
	if opt.HasTimestamp {
		encoderConfig.EncodeTime = func(t time.Time, enc zapcore.PrimitiveArrayEncoder) {
			// nanos := t.Unix()
			// sec := float64(nanos) / float64(time.Second)
			enc.AppendInt(int(t.UnixMilli()))
		}
	}
	if opt.Formatter == JSON {
		encoder = zapcore.NewJSONEncoder(encoderConfig)
	} else {
		encoder = zapcore.NewConsoleEncoder(encoderConfig)
	}

	switch opt.Level {
	case "info":
		level = zapcore.InfoLevel
	case "debug":
		level = zapcore.DebugLevel
	case "warn":
		level = zapcore.WarnLevel
	case "Error":
		level = zapcore.ErrorLevel
	case "panic": //会跳出来
		level = zapcore.DPanicLevel
	default:
		level = zapcore.DebugLevel
	}

	var hook lumberjack.Logger
	if opt.OutType == FILE || opt.OutType == ALL {
		var (
			Filename   string = "./web.log" // 日志文件路径，默认 os.TempDir()
			MaxSize    int    = 10          // 每个日志文件保存10M，默认 100M
			MaxBackups int    = 30          // 保留30个备份，默认不限
			MaxAge     int    = 7           // 保留7天，默认不限
			Compress   bool   = false       // 是否压缩，默认不压缩
		)

		if opt.Filename != "" {
			Filename = opt.Filename
		}
		if opt.MaxAge != 0 {
			MaxAge = opt.MaxAge
		}

		if opt.MaxBackups != 0 {
			MaxBackups = opt.MaxBackups
		}

		if opt.MaxSize != 0 {
			MaxSize = opt.MaxSize
		}

		if opt.Compress {
			Compress = true
		}

		hook = lumberjack.Logger{
			Filename:   Filename,   // 日志文件路径，默认 os.TempDir()
			MaxSize:    MaxSize,    // 每个日志文件保存10M，默认 100M
			MaxBackups: MaxBackups, // 保留30个备份，默认不限
			MaxAge:     MaxAge,     // 保留7天，默认不限
			Compress:   Compress,   // 是否压缩，默认不压缩
		}
	}

	switch opt.OutType {
	case CONSOLE, "":
		write = zapcore.AddSync(os.Stdout)
	case FILE:
		write = zapcore.AddSync(&hook)
	case ALL:
		write = zapcore.NewMultiWriteSyncer(zapcore.AddSync(os.Stdout), zapcore.AddSync(&hook))
	}

	//log
	core := zapcore.NewCore(encoder, write, level)

	ops := []zap.Option{}
	if opt.Caller {
		ops = append(ops, zap.AddCaller())
	}
	if opt.Development {
		ops = append(ops, zap.Development())
	}
	// if opt.HasTimestamp{
	// 	ops = append(ops, zap.Int("serviceName", time.Now().Unix()))
	// }

	log := Wrap(zap.New(core, ops...))
	return log
}
