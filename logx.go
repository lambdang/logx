package logx

import (
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

/*
对log进行包装
提供三个函数
xxx()
xxxw()
wwwf()

	case "info":
		level = zapcore.InfoLevel
	case "debug":
		level = zapcore.DebugLevel
	case "warn":
		level = zapcore.WarnLevel
	case "Error":
		level = zapcore.ErrorLevel
	case "panic": //会跳出来
		level = zapcore.DPanicLevel
*/
type MyLog struct {
	*zap.Logger
}

//info
func (this *MyLog) Infow(msg string, args ...interface{}) {
	this.Sugar().Infow(msg, args...)
}
func (this *MyLog) Infof(template string, args ...interface{}) {
	this.Sugar().Infof(template, args...)
}

//debug
func (this *MyLog) Debugw(msg string, args ...interface{}) {
	this.Sugar().Debugw(msg, args...)
}
func (this *MyLog) Debugf(template string, args ...interface{}) {
	this.Sugar().Debugf(template, args...)
}

//warn
func (this *MyLog) Warnw(msg string, args ...interface{}) {
	this.Sugar().Warnw(msg, args...)
}
func (this *MyLog) Warnf(template string, args ...interface{}) {
	this.Sugar().Warnf(template, args...)
}

//error
func (this *MyLog) Errorw(msg string, args ...interface{}) {
	this.Sugar().Debugw(msg, args...)
}
func (this *MyLog) Errorf(template string, args ...interface{}) {
	this.Sugar().Debugf(template, args...)
}

//panic
func (this *MyLog) Panicw(msg string, args ...interface{}) {
	this.Sugar().DPanicw(msg, args...)
}
func (this *MyLog) Panicf(template string, args ...interface{}) {
	this.Sugar().DPanicf(template, args...)
}

func Wrap(log *zap.Logger) *MyLog {
	return &MyLog{log}
}

//with
func (this *MyLog) With(fields ...zapcore.Field) *MyLog {
	return &MyLog{this.Logger.With(fields...)}
}
