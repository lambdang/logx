package logx

const (
	ERROR = "error"
	DEBUG = "debug"
	PANIC = "panic"
	INFO  = "info"
	WARN  = "warn"

	JSON = "json"
	TXT  = "txt"

	CONSOLE = "console"
	FILE    = "file"
	ALL     = "all"
)
