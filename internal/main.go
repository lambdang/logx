package main

import (
	"errors"
	"logx"

	"go.uber.org/zap"
)

func main() {
	opt := logx.Options{}
	opt.Filename = "text.log"
	opt.OutType = logx.ALL

	lg := logx.New(opt)
	lg.Info("this is test info", zap.Any("err", errors.New("这是测试")))
	lg.Infof("%s is test infof", "abc")
	lg.Infow("this is test info", "a", "good")
}
